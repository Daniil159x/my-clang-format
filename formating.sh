#!/bin/bash

set -e

cd before

for i in *.cpp; do
    clang-format $i > ../after/$i
done;
